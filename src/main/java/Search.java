import java.util.*;

public class Search {

    private Graph graph;

    public Search(Graph graph) {
        this.graph = graph;
    }

    private int[] discoveredArray() {
        int[] discovered = new int[graph.n];
        for (int i = 0; i < graph.n; i++) {
            discovered[i] = -1;
        }
        return discovered;
    }

    private void showPath(int[] discovered, int last) {
        System.out.println();
        System.out.println("Path:");

        while (discovered[last] != -1) {
            System.out.print(graph.getCityByIndex(last));
            last = discovered[last];
            System.out.print(" <- ");
            if (last == discovered[last]) {
                System.out.print(graph.getCityByIndex(last));
                return;
            }
        }
    }

    public void BFS(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("BFS: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        Queue<Integer> queue = new ArrayDeque<>();

        int[] discovered = discoveredArray();

        discovered[input] = input;
        queue.add(input);

        while (!queue.isEmpty()) {
            input = queue.poll();

            for (int i = 0; i < graph.n; i++) {
                if (graph.matrix[input][i] > 0) {
                    if (discovered[i] == -1) {
                        discovered[i] = input;
                        System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[input][i]);
                        if (i == output) {
                            discovered[i] = input;
                            showPath(discovered, i);
                            return;
                        }
                        queue.add(i);
                    }
                }
            }
            System.out.println();

        }
        System.out.println("No solution");

    }

    public void DFS(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("DFS: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        int[] discovered = discoveredArray();

        discovered[input] = input;

        if (!RecursionDFS(input, output, discovered)) {
            System.out.println("No solution");
        } else {
            showPath(discovered, output);
        }


    }

    private boolean RecursionDFS(int input, int output, int[] discovered) {
        for (int i = 0; i < graph.n; i++) {
            if (graph.matrix[input][i] > 0) {
                if (discovered[i] == -1) {
                    discovered[i] = input;

                    System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[input][i]);

                    if (i == output) {
                        return true;
                    }
                    if (RecursionDFS(i, output, discovered)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean DLS(String from, String to, int limit) {
        System.out.println();
        System.out.println();
        System.out.println("DLS: " + from + " -> " + to + ", limit = " + limit + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        int[] discovered = discoveredArray();
        discovered[input] = input;

        if (RecursionDLS(input, output, discovered, 1, limit)) {
            showPath(discovered, output);
            return true;
        }
        System.out.println("No solution");
        return false;
    }

    private boolean RecursionDLS(int input, int output, int[] discovered, int depth, int limit) {
        if (depth > limit) {
            return false;
        }
        for (int i = 0; i < graph.n; i++) {
            if (graph.matrix[input][i] > 0) {
                if (discovered[i] == -1) {
                    discovered[i] = input;
                    System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[input][i]);
                    if (i == output) {
                        return true;
                    }
                    if (RecursionDLS(i, output, discovered, depth + 1, limit)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int IDDLS(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("IDDLS: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        int limit = 1;
        while (!DLS(from, to, limit)) {
            if (limit >= graph.edges.size()) {
                System.out.println("No solution");
                return -1;
            }
            limit++;
        }
        System.out.println();
        System.out.println("IDDLS depth = " + limit);
        return limit;

    }


    public void BDS(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("BDS: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        Queue<Integer> queueBegin = new ArrayDeque<>();
        Queue<Integer> queueEnd = new ArrayDeque<>();
        int[] discovered = new int[graph.n];
        int[] discoveredBegin = discoveredArray();
        int[] discoveredEnd = discoveredArray();

        discovered[input] = 1;
        discoveredBegin[input] = input;
        queueBegin.add(input);

        discovered[output] = 2;

        discoveredEnd[output] = output;

        queueEnd.add(output);

        while (!queueBegin.isEmpty() || !queueEnd.isEmpty()) {
            if (!queueBegin.isEmpty()) {
                input = queueBegin.poll();
                for (int i = 0; i < graph.n; i++) {
                    if (graph.matrix[input][i] > 0) {
                        if (discovered[i] == 0) {
                            discovered[i] = 1;
                            discoveredBegin[i] = input;
                            System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[input][i]);
                            queueBegin.add(i);
                        } else if (discovered[i] == 2) {
                            System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[input][i]);
                            discoveredBegin[i] = input;

                            int last = i;
                            while ((discoveredEnd[last] != last)&&(discoveredEnd[last] != -1)) {
                                discoveredBegin[discoveredEnd[last]] = last;
                                last = discoveredEnd[last];
                            }
                            showPath(discoveredBegin, last);
                            return;
                        }
                    }
                }
                System.out.println();
            }

            if (!queueEnd.isEmpty()) {
                output = queueEnd.poll();
                for (int i = 0; i < graph.n; i++) {
                    if (graph.matrix[output][i] > 0) {
                        if (discovered[i] == 0) {
                            discovered[i] = 2;
                            discoveredEnd[i] = output;
                            System.out.println(graph.getCityByIndex(output) + " <- " + graph.getCityByIndex(i) + " " + graph.matrix[output][i]);
                            queueEnd.add(i);
                        } else if (discovered[i] == 1) {
                            System.out.println(graph.getCityByIndex(output) + " <- " + graph.getCityByIndex(i) + " " + graph.matrix[output][i]);

                            discoveredEnd[i] = output;

                            int last = i;
                            while ((discoveredEnd[last] != last)&&(discoveredEnd[last] != -1)) {
                                discoveredBegin[discoveredEnd[last]] = last;
                                last = discoveredEnd[last];
                            }

                            showPath(discoveredBegin, last);
                            return;
                        }
                    }
                }
                System.out.println();
            }

        }
        System.out.println("No solution");

    }

    public void BestFirstSearch(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("InformFirstMatch: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        int[] discovered = discoveredArray();

        Comparator<Integer> distanceCompare = new Comparator<Integer>() {
            @Override
            public int compare(Integer s1, Integer s2) {
                return graph.getDistanceByIndex(s1) - graph.getDistanceByIndex(s2);
            }
        };

        PriorityQueue<Integer> queue = new PriorityQueue<>(distanceCompare);


        discovered[input] = input;

        queue.add(input);

        while (!queue.isEmpty()) {
            input = queue.poll();
            System.out.println(graph.getCityByIndex(input) + " " + graph.matrix[input][input] + " (" + graph.getDistanceByIndex(input) + ")");

            for (int i = 0; i < graph.n; i++) {
                if (i != input && graph.matrix[input][i] > 0) {
                    if (discovered[i] == -1) {
                        discovered[i] = input;
                        graph.matrix[i][i] = graph.matrix[input][input] + graph.matrix[input][i];
                        System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " + graph.matrix[i][i] + " (" + graph.getDistanceByIndex(i) + ")");
                        if (i == output) {
                            System.out.println("Total " + graph.matrix[i][i]);
                            showPath(discovered, output);
                            return;
                        }
                        queue.add(i);
                    }
                }
            }
            System.out.println();

        }
        System.out.println("No solution");

    }

    public void InformMinimumMatch(String from, String to) {
        System.out.println();
        System.out.println();
        System.out.println("InformMinimumMatch: " + from + " -> " + to + ":");
        System.out.println();

        Integer input = graph.getCityIndex(from);
        Integer output = graph.getCityIndex(to);

        int[] discovered = discoveredArray();

        Comparator<Integer> distanceCompare = new Comparator<Integer>() {
            @Override
            public int compare(Integer s1, Integer s2) {
                return (graph.matrix[s1][s1] + graph.getDistanceByIndex(s1)) - (graph.matrix[s2][s2] + graph.getDistanceByIndex(s2));
            }
        };

        PriorityQueue<Integer> queue = new PriorityQueue<>(distanceCompare);


        discovered[input] = input;

        queue.add(input);

        while (!queue.isEmpty()) {
            input = queue.poll();
            System.out.println(graph.getCityByIndex(input) + " " + graph.matrix[input][input] + " + (" + graph.getDistanceByIndex(input) + ")");

            for (int i = 0; i < graph.n; i++) {
                if (i != input && graph.matrix[input][i] > 0) {
                    if (discovered[i] == -1) {
                        discovered[i] = input;
                        graph.matrix[i][i] = graph.matrix[input][input] + graph.matrix[input][i];
                        System.out.println(graph.getCityByIndex(input) + " -> " + graph.getCityByIndex(i) + " " +
                                graph.matrix[input][i] + " + (" + graph.getDistanceByIndex(i) + ") = "
                                + (graph.getDistanceByIndex(i) + graph.matrix[input][i]));
                        if (i == output) {
                            System.out.println(graph.getCityByIndex(i) + " " + graph.matrix[i][i]);
                            showPath(discovered, output);
                            return;
                        }
                        queue.add(i);
                    }
                }
            }
            System.out.println();

        }
        System.out.println("No solution");

    }


}

