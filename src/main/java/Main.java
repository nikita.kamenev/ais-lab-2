import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String args[]){
        Graph graph = GraphReader.readGraph("./src/main/resources/input.txt");

        Search search = new Search(graph);
        search.BFS("Самара", "Ярославль");
        search.DFS("Самара", "Ярославль");
        search.DLS("Самара", "Ярославль", 5);
        search.IDDLS("Самара", "Ярославль");
        search.BDS("Самара", "Ярославль");

        GraphReader.readDistance("./src/main/resources/Ярославль.txt", graph);
        search.BestFirstSearch("Самара", "Ярославль");
        search.InformMinimumMatch("Самара", "Ярославль");




    }
}
