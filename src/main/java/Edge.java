

public class Edge {
    public Integer input, output, weight;

    public Edge(Integer input, Integer output, Integer weight) {

        this.input = input;
        this.output = output;
        this.weight = weight;
    }

}
