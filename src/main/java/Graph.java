import java.util.ArrayList;
import java.util.List;

public class Graph {

    public int n;
    public List<String> cities = new ArrayList();
    public int[] target;
    public int[][] matrix;
    public List<Edge> edges = new ArrayList<>();

    public void setEdges(List<Edge> edges) {
        this.n = cities.size();
        this.edges.addAll(edges);
        matrix = new int[n][n];
        target = new int[n];
        for (Edge edge : edges) {
            int src = edge.input;
            int dest = edge.output;
            matrix[src][dest] = edge.weight;
            matrix[dest][src] = edge.weight;
        }
    }

    public void setDistanceByIndex(int index, int distance){
        target[index] = distance;
    }

    public Integer getDistanceByIndex(int index){
        return target[index];
    }

    public Integer getCityIndex(String city){
        if(!this.cities.contains(city)){
            this.cities.add(city);
        }
        return this.cities.indexOf(city);
    }

    public String getCityByIndex(Integer index){
        return this.cities.size() > index && index >= 0 ? this.cities.get(index) : null;
    }

    public void printMatrix(){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                System.out.print(String.format("%1$6d", matrix[i][j]));
            }
            System.out.println();
        }
    }

}
