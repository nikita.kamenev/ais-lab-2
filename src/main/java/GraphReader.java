import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GraphReader {

    public static Graph readGraph(String fileName) {
        Graph graph = new Graph();
        List<Edge> edges = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new FileReader(fileName));
            while (sc.hasNextLine()) {
                String edgeLine = sc.nextLine();
                String[] splitted = edgeLine.split(" ");
                Edge edge = new Edge(graph.getCityIndex(splitted[0]), graph.getCityIndex(splitted[1]), Integer.valueOf(splitted[2]));
                edges.add(edge);
            }
        } catch(FileNotFoundException e){
            System.out.println("Uncorrect file name");
        }
        graph.setEdges(edges);
        return graph;
    }

    public static void readDistance(String fileName, Graph graph){
        try {
            Scanner sc = new Scanner(new FileReader(fileName));
            while (sc.hasNextLine()) {
                String distanceLine = sc.nextLine();
                String[] splitted = distanceLine.split(" ");
                graph.setDistanceByIndex(graph.getCityIndex(splitted[0]), Integer.valueOf(splitted[1]));
            }
        } catch(FileNotFoundException e){
            System.out.println("Uncorrect file name");
        }
    }


}
